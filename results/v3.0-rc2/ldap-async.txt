# Run progress: 0.00% complete, ETA 01:30:36
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.AuthorizationCodeGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 50.619 ops/s
# Warmup Iteration   2: 81.702 ops/s
Iteration   1: 262.945 ops/s
Iteration   2: 328.064 ops/s
Iteration   3: 315.734 ops/s
Iteration   4: 322.537 ops/s
Iteration   5: 341.487 ops/s

Result: 314.154 ?(99.9%) 116.092 ops/s [Average]
  Statistics: (min, avg, max) = (262.945, 314.154, 341.487), stdev = 30.149
  Confidence interval (99.9%): [198.062, 430.245]


# Run progress: 5.56% complete, ETA 01:37:29
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ClientRegistrationBenchmark.register
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 0.683 ops/ms
# Warmup Iteration   2: 0.601 ops/ms
Iteration   1: 1.434 ops/ms
Iteration   2: 1.512 ops/ms
Iteration   3: 1.500 ops/ms
Iteration   4: 1.445 ops/ms
Iteration   5: 1.636 ops/ms

Result: 1.505 ?(99.9%) 0.310 ops/ms [Average]
  Statistics: (min, avg, max) = (1.434, 1.505, 1.636), stdev = 0.080
  Confidence interval (99.9%): [1.196, 1.815]


# Run progress: 11.11% complete, ETA 01:26:19
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ImplicitGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 374.269 ops/s
# Warmup Iteration   2: 378.085 ops/s

Iteration   1: 327.008 ops/s
Iteration   2: 330.438 ops/s
Iteration   3: 352.438 ops/s
Iteration   4: 373.069 ops/s
Iteration   5: 383.655 ops/s

Result: 353.322 ?(99.9%) 96.783 ops/s [Average]
  Statistics: (min, avg, max) = (327.008, 353.322, 383.655), stdev = 25.134
  Confidence interval (99.9%): [256.539, 450.104]


# Run progress: 16.67% complete, ETA 01:22:22
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ShortLivedAuthorizationCodeGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 348.049 ops/s
# Warmup Iteration   2: 342.052 ops/s
Iteration   1: 352.087 ops/s
Iteration   2: 372.437 ops/s
Iteration   3: 359.651 ops/s
Iteration   4: 376.075 ops/s
Iteration   5: 348.399 ops/s

Result: 361.730 ?(99.9%) 46.982 ops/s [Average]
  Statistics: (min, avg, max) = (348.399, 361.730, 376.075), stdev = 12.201
  Confidence interval (99.9%): [314.748, 408.711]


# Run progress: 22.22% complete, ETA 01:15:23
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ShortLivedImplicitGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 422.209 ops/s
# Warmup Iteration   2: 423.466 ops/s
Iteration   1: 393.415 ops/s
Iteration   2: 399.412 ops/s
Iteration   3: 389.286 ops/s
Iteration   4: 402.436 ops/s
Iteration   5: 382.347 ops/s

Result: 393.379 ?(99.9%) 30.858 ops/s [Average]
  Statistics: (min, avg, max) = (382.347, 393.379, 402.436), stdev = 8.014
  Confidence interval (99.9%): [362.522, 424.237]



# Run progress: 27.78% complete, ETA 01:09:10
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addAndDeleteSubjectSessions
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: <failure>

java.lang.IllegalArgumentException: The value must not be null
        at com.nimbusds.common.id.BaseIdentifier.<init>(BaseIdentifier.java:57)
        at com.nimbusds.common.id.SID.<init>(SID.java:27)
        at com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addSubjectSession(SubjectSessionBenchmark.java:52)
        at com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addAndDeleteSubjectSessions(SubjectSessionBenchmark.java:87)
        at com.nimbusds.openid.connect.provider.benchmarks.generated.SubjectSessionBenchmark_addAndDeleteSubjectSessions.addAndDeleteSubjectSessions_Throughput(SubjectSessionBenchmark_addAndDeleteSubjectSessions.java:66)
        at sun.reflect.GeneratedMethodAccessor10.invoke(Unknown Source)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.openjdk.jmh.runner.LoopBenchmarkHandler$BenchmarkTask.call(LoopBenchmarkHandler.java:198)
        at org.openjdk.jmh.runner.LoopBenchmarkHandler$BenchmarkTask.call(LoopBenchmarkHandler.java:180)
        at java.util.concurrent.FutureTask.run(FutureTask.java:262)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
        at java.lang.Thread.run(Thread.java:745)



# Run progress: 33.33% complete, ETA 00:53:14
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.AuthorizationCodeGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 0.083 ?(99.9%) 0.006 s/op
# Warmup Iteration   2: 0.090 ?(99.9%) 0.005 s/op
Iteration   1: 0.083 ?(99.9%) 0.001 s/op
Iteration   2: 0.075 ?(99.9%) 0.001 s/op
Iteration   3: 0.076 ?(99.9%) 0.001 s/op
Iteration   4: 0.081 ?(99.9%) 0.001 s/op
Iteration   5: 0.081 ?(99.9%) 0.001 s/op

Result: 0.079 ?(99.9%) 0.014 s/op [Average]
  Statistics: (min, avg, max) = (0.075, 0.079, 0.083), stdev = 0.004
  Confidence interval (99.9%): [0.066, 0.093]


# Run progress: 38.89% complete, ETA 00:50:46
# Warmup: 2 iterations, 1 s each

# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ClientRegistrationBenchmark.register
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 14.752 ?(99.9%) 0.732 ms/op
# Warmup Iteration   2: 19.653 ?(99.9%) 0.737 ms/op
Iteration   1: 17.017 ?(99.9%) 0.135 ms/op
Iteration   2: 16.266 ?(99.9%) 0.128 ms/op
Iteration   3: 18.553 ?(99.9%) 0.163 ms/op
Iteration   4: 15.382 ?(99.9%) 0.113 ms/op
Iteration   5: 20.674 ?(99.9%) 0.218 ms/op

Result: 17.578 ?(99.9%) 8.030 ms/op [Average]
  Statistics: (min, avg, max) = (15.382, 17.578, 20.674), stdev = 2.085
  Confidence interval (99.9%): [9.548, 25.608]


# Run progress: 44.44% complete, ETA 00:46:42
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ImplicitGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 0.072 ?(99.9%) 0.006 s/op
# Warmup Iteration   2: 0.073 ?(99.9%) 0.005 s/op
Iteration   1: 0.083 ?(99.9%) 0.001 s/op
Iteration   2: 0.070 ?(99.9%) 0.001 s/op
Iteration   3: 0.072 ?(99.9%) 0.001 s/op
Iteration   4: 0.068 ?(99.9%) 0.000 s/op
Iteration   5: 0.073 ?(99.9%) 0.001 s/op

Result: 0.073 ?(99.9%) 0.022 s/op [Average]
  Statistics: (min, avg, max) = (0.068, 0.073, 0.083), stdev = 0.006
  Confidence interval (99.9%): [0.051, 0.095]


# Run progress: 50.00% complete, ETA 00:43:13
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ShortLivedAuthorizationCodeGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 0.097 ?(99.9%) 0.007 s/op
# Warmup Iteration   2: 0.075 ?(99.9%) 0.005 s/op
Iteration   1: 0.081 ?(99.9%) 0.001 s/op
Iteration   2: 0.080 ?(99.9%) 0.001 s/op

Iteration   3: 0.077 ?(99.9%) 0.001 s/op
Iteration   4: 0.075 ?(99.9%) 0.001 s/op
Iteration   5: 0.072 ?(99.9%) 0.001 s/op

Result: 0.077 ?(99.9%) 0.015 s/op [Average]
  Statistics: (min, avg, max) = (0.072, 0.077, 0.081), stdev = 0.004
  Confidence interval (99.9%): [0.062, 0.092]


# Run progress: 55.56% complete, ETA 00:38:37
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ShortLivedImplicitGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: 0.061 ?(99.9%) 0.005 s/op
# Warmup Iteration   2: 0.055 ?(99.9%) 0.003 s/op
Iteration   1: 0.062 ?(99.9%) 0.001 s/op
Iteration   2: 0.061 ?(99.9%) 0.001 s/op
Iteration   3: 0.063 ?(99.9%) 0.001 s/op
Iteration   4: 0.064 ?(99.9%) 0.001 s/op
Iteration   5: 0.063 ?(99.9%) 0.001 s/op

Result: 0.062 ?(99.9%) 0.004 s/op [Average]
  Statistics: (min, avg, max) = (0.061, 0.062, 0.064), stdev = 0.001
  Confidence interval (99.9%): [0.059, 0.066]


# Run progress: 61.11% complete, ETA 00:33:56
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Average time, time/op
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addAndDeleteSubjectSessions
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: <failure>

java.lang.IllegalArgumentException: The value must not be null
        at com.nimbusds.common.id.BaseIdentifier.<init>(BaseIdentifier.java:57)
        at com.nimbusds.common.id.SID.<init>(SID.java:27)
        at com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addSubjectSession(SubjectSessionBenchmark.java:52)
        at com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addAndDeleteSubjectSessions(SubjectSessionBenchmark.java:87)
        at com.nimbusds.openid.connect.provider.benchmarks.generated.SubjectSessionBenchmark_addAndDeleteSubjectSessions.addAndDeleteSubjectSessions_AverageTime(SubjectSessionBenchmark_addAndDeleteSubjectSessions.java:123)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.openjdk.jmh.runner.LoopBenchmarkHandler$BenchmarkTask.call(LoopBenchmarkHandler.java:198)
        at org.openjdk.jmh.runner.LoopBenchmarkHandler$BenchmarkTask.call(LoopBenchmarkHandler.java:180)

        at java.util.concurrent.FutureTask.run(FutureTask.java:262)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
        at java.lang.Thread.run(Thread.java:745)



# Run progress: 66.67% complete, ETA 00:26:40
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Sampling time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.AuthorizationCodeGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: n = 283, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
# Warmup Iteration   2: n = 289, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
Iteration   1: n = 19235, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
Iteration   2: n = 18205, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
Iteration   3: n = 17404, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 2, 2, 2 s/op
Iteration   4: n = 18811, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 1, 1 s/op
Iteration   5: n = 19249, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op

Result: 0.081 ?(99.9%) 0.001 s/op [Average]
  Statistics: (min, avg, max) = (0.017, 0.081, 2.231), stdev = 0.048
  Confidence interval (99.9%): [0.080, 0.081]
  Samples, N = 92904
        mean =      0.081 ?(99.9%) 0.001 s/op
         min =      0.017 s/op
  p( 0.0000) =      0.017 s/op
  p(50.0000) =      0.075 s/op
  p(90.0000) =      0.117 s/op
  p(95.0000) =      0.133 s/op
  p(99.0000) =      0.181 s/op
  p(99.9000) =      0.407 s/op
  p(99.9900) =      2.185 s/op
  p(99.9990) =      2.231 s/op
  p(99.9999) =      2.231 s/op
         max =      2.231 s/op


# Run progress: 72.22% complete, ETA 00:22:46
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Sampling time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ClientRegistrationBenchmark.register
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: n = 1847, mean = 14 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 2, 13, 24, 30, 41, 52, 61, 61 ms/op
# Warmup Iteration   2: n = 1457, mean = 17 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 2, 14, 29, 42, 76, 117, 118, 118 ms/op
Iteration   1: n = 98001, mean = 15 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 2, 13, 25, 31, 50, 220, 664, 678 ms/op

Iteration   2: n = 79194, mean = 19 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 1, 13, 24, 30, 47, 111, 13187, 13204 ms/op
Iteration   3: n = 89611, mean = 17 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 2, 13, 27, 33, 52, 432, 1674, 1682 ms/op
Iteration   4: n = 93692, mean = 16 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 2, 13, 26, 32, 53, 618, 968, 981 ms/op
Iteration   5: n = 64524, mean = 24 ms/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 2, 12, 23, 29, 47, 1637, 19629, 19629 ms/op

Result: 17.710 ?(99.9%) 0.929 ms/op [Average]
  Statistics: (min, avg, max) = (1.483, 17.710, 19629.343), stdev = 184.128
  Confidence interval (99.9%): [16.781, 18.640]
  Samples, N = 425022
        mean =     17.710 ?(99.9%) 0.929 ms/op
         min =      1.483 ms/op
  p( 0.0000) =      1.483 ms/op
  p(50.0000) =     12.632 ms/op
  p(90.0000) =     25.035 ms/op
  p(95.0000) =     31.359 ms/op
  p(99.0000) =     50.266 ms/op
  p(99.9000) =    535.286 ms/op
  p(99.9900) =  13186.892 ms/op
  p(99.9990) =  19629.343 ms/op
  p(99.9999) =  19629.343 ms/op
         max =  19629.343 ms/op


# Run progress: 77.78% complete, ETA 00:18:22
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Sampling time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ImplicitGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: n = 381, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
# Warmup Iteration   2: n = 347, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
Iteration   1: n = 8564, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 19, 20, 20 s/op
Iteration   2: n = 7963, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 6, 19, 19, 19 s/op
Iteration   3: n = 8758, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 6, 17, 17, 17 s/op
Iteration   4: n = 9122, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 24, 24, 24 s/op
Iteration   5: n = 6799, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 8, 17, 18, 18 s/op

Result: 0.185 ?(99.9%) 0.022 s/op [Average]
  Statistics: (min, avg, max) = (0.013, 0.185, 23.790), stdev = 1.331
  Confidence interval (99.9%): [0.164, 0.207]
  Samples, N = 41206
        mean =      0.185 ?(99.9%) 0.022 s/op
         min =      0.013 s/op
  p( 0.0000) =      0.013 s/op
  p(50.0000) =      0.061 s/op
  p(90.0000) =      0.101 s/op
  p(95.0000) =      0.119 s/op
  p(99.0000) =      1.060 s/op
  p(99.9000) =     19.428 s/op
  p(99.9900) =     23.786 s/op

  p(99.9990) =     23.790 s/op
  p(99.9999) =     23.790 s/op
         max =     23.790 s/op


# Run progress: 83.33% complete, ETA 00:13:58
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Sampling time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ShortLivedAuthorizationCodeGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: n = 349, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
# Warmup Iteration   2: n = 182, mean = 3 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 18, 18, 18, 18, 18, 18 s/op
Iteration   1: n = 9235, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 8, 19, 19, 19 s/op
Iteration   2: n = 7754, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 18, 18, 18 s/op
Iteration   3: n = 9025, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 7, 17, 17, 17 s/op
Iteration   4: n = 8433, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 5, 17, 17, 17 s/op
Iteration   5: n = 11754, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 5, 11, 11, 11 s/op

Result: 0.174 ?(99.9%) 0.019 s/op [Average]
  Statistics: (min, avg, max) = (0.012, 0.174, 18.891), stdev = 1.226
  Confidence interval (99.9%): [0.155, 0.193]
  Samples, N = 46201
        mean =      0.174 ?(99.9%) 0.019 s/op
         min =      0.012 s/op
  p( 0.0000) =      0.012 s/op
  p(50.0000) =      0.054 s/op
  p(90.0000) =      0.084 s/op
  p(95.0000) =      0.097 s/op
  p(99.0000) =      5.184 s/op
  p(99.9000) =     17.650 s/op
  p(99.9900) =     18.858 s/op
  p(99.9990) =     18.891 s/op
  p(99.9999) =     18.891 s/op
         max =     18.891 s/op


# Run progress: 88.89% complete, ETA 00:09:27
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Sampling time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.ShortLivedImplicitGrantBenchmark.authorize
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: n = 551, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
# Warmup Iteration   2: n = 474, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 0, 0, 0, 0 s/op
Iteration   1: n = 8564, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 5, 17, 17, 17 s/op
Iteration   2: n = 9673, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 5, 17, 17, 17 s/op
Iteration   3: n = 9638, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 5, 19, 19, 19 s/op

Iteration   4: n = 9299, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 8, 17, 17, 17 s/op
Iteration   5: n = 11962, mean = 0 s/op, p{0.00, 0.50, 0.90, 0.95, 0.99, 0.999, 0.9999, 1.00} = 0, 0, 0, 0, 5, 13, 13, 13 s/op

Result: 0.171 ?(99.9%) 0.017 s/op [Average]
  Statistics: (min, avg, max) = (0.011, 0.171, 18.891), stdev = 1.141
  Confidence interval (99.9%): [0.154, 0.188]
  Samples, N = 49136
        mean =      0.171 ?(99.9%) 0.017 s/op
         min =      0.011 s/op
  p( 0.0000) =      0.011 s/op
  p(50.0000) =      0.051 s/op
  p(90.0000) =      0.096 s/op
  p(95.0000) =      0.217 s/op
  p(99.0000) =      5.218 s/op
  p(99.9000) =     17.377 s/op
  p(99.9900) =     18.860 s/op
  p(99.9990) =     18.891 s/op
  p(99.9999) =     18.891 s/op
         max =     18.891 s/op


# Run progress: 94.44% complete, ETA 00:04:47
# Warmup: 2 iterations, 1 s each
# Measurement: 5 iterations, 60 s each
# Timeout: 10 min per iteration
# Threads: 25 threads, will synchronize iterations
# Benchmark mode: Sampling time
# Benchmark: com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addAndDeleteSubjectSessions
# Fork: N/A, test runs in the existing VM
# Warmup Iteration   1: <failure>

java.lang.IllegalArgumentException: The value must not be null
        at com.nimbusds.common.id.BaseIdentifier.<init>(BaseIdentifier.java:57)
        at com.nimbusds.common.id.SID.<init>(SID.java:27)
        at com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addSubjectSession(SubjectSessionBenchmark.java:52)
        at com.nimbusds.openid.connect.provider.benchmarks.SubjectSessionBenchmark.addAndDeleteSubjectSessions(SubjectSessionBenchmark.java:87)
        at com.nimbusds.openid.connect.provider.benchmarks.generated.SubjectSessionBenchmark_addAndDeleteSubjectSessions.addAndDeleteSubjectSessions_SampleTime(SubjectSessionBenchmark_addAndDeleteSubjectSessions.java:180)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.openjdk.jmh.runner.LoopBenchmarkHandler$BenchmarkTask.call(LoopBenchmarkHandler.java:198)
        at org.openjdk.jmh.runner.LoopBenchmarkHandler$BenchmarkTask.call(LoopBenchmarkHandler.java:180)
        at java.util.concurrent.FutureTask.run(FutureTask.java:262)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
        at java.lang.Thread.run(Thread.java:745)



# Run complete. Total time: 01:21:21

Benchmark                                                            Mode  Samples    Score     Error   Units
c.n.o.c.p.b.AuthorizationCodeGrantBenchmark.authorize               thrpt        5  314.154 ? 116.092   ops/s
c.n.o.c.p.b.ClientRegistrationBenchmark.register                    thrpt        5    1.505 ?   0.310  ops/ms
c.n.o.c.p.b.ImplicitGrantBenchmark.authorize                        thrpt        5  353.322 ?  96.783   ops/s
c.n.o.c.p.b.ShortLivedAuthorizationCodeGrantBenchmark.authorize     thrpt        5  361.730 ?  46.982   ops/s
c.n.o.c.p.b.ShortLivedImplicitGrantBenchmark.authorize              thrpt        5  393.379 ?  30.858   ops/s
c.n.o.c.p.b.AuthorizationCodeGrantBenchmark.authorize                avgt        5    0.079 ?   0.014    s/op
c.n.o.c.p.b.ClientRegistrationBenchmark.register                     avgt        5   17.578 ?   8.030   ms/op
c.n.o.c.p.b.ImplicitGrantBenchmark.authorize                         avgt        5    0.073 ?   0.022    s/op
c.n.o.c.p.b.ShortLivedAuthorizationCodeGrantBenchmark.authorize      avgt        5    0.077 ?   0.015    s/op
c.n.o.c.p.b.ShortLivedImplicitGrantBenchmark.authorize               avgt        5    0.062 ?   0.004    s/op
c.n.o.c.p.b.AuthorizationCodeGrantBenchmark.authorize              sample    92904    0.081 ?   0.001    s/op
c.n.o.c.p.b.ClientRegistrationBenchmark.register                   sample   425022   17.710 ?   0.929   ms/op
c.n.o.c.p.b.ImplicitGrantBenchmark.authorize                       sample    41206    0.185 ?   0.022    s/op
c.n.o.c.p.b.ShortLivedAuthorizationCodeGrantBenchmark.authorize    sample    46201    0.174 ?   0.019    s/op
c.n.o.c.p.b.ShortLivedImplicitGrantBenchmark.authorize             sample    49136    0.171 ?   0.017    s/op
