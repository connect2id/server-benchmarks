package com.nimbusds.openid.connect.provider.benchmarks;


/**
 * Base abstract benchmark.
 */
abstract class AbstractBenchmark {


	/**
	 * The Connect2id server details.
	 */
	final static ServerDetails SERVER_DETAILS = ServerDetails.fromSystemProperties();
}
