package com.nimbusds.openid.connect.provider.benchmarks;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import com.nimbusds.oauth2.sdk.token.BearerAccessToken;


/**
 * Connect2id server details.
 */
public final class ServerDetails {


	/**
	 * The base server URI.
	 */
	private final URI baseURI;


	/**
	 * The client registration endpoint.
	 */
	public final URI clientRegEndpoint;


	/**
	 * The Connect2id server authorisation session endpoint (for the login
	 * page).
	 */
	public final URI authzSessionEndpoint;


	/**
	 * The Connect2id server subject session store endpoint.
	 */
	public final URI subjectSessionEndpoint;


	/**
	 * The token endpoint.
	 */
	public final URI tokenEndpoint;


	/**
	 * The UserInfo endpoint.
	 */
	public final URI userInfoEndpoint;


	/**
	 * Access token for the Connect2id server integration web APIs.
	 */
	public final BearerAccessToken apiAccessToken;


	/**
	 * Creates a new Connect2id server details instance.
	 *
	 * @param serverURI      The base server URI. Must not be {@code null}.
	 * @param apiAccessToken The Bearer access token for the Connect2id
	 *                       server integration web API.
	 */
	private ServerDetails(final URI serverURI, final BearerAccessToken apiAccessToken) {

		baseURI = serverURI;

		try {
			clientRegEndpoint = new URI(serverURI + "/clients");
			authzSessionEndpoint = new URI(serverURI + "/authz-sessions/rest/v2");
			subjectSessionEndpoint = new URI(serverURI + "/session-store/rest/v2");
			tokenEndpoint = new URI(serverURI + "/token");
			userInfoEndpoint = new URI(serverURI + "/userinfo");
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		this.apiAccessToken = apiAccessToken;
	}


	/**
	 * Retrieves the Connect2id server details from system properties.
	 *
	 * <p>Keys:
	 *
	 * <ul>
	 *     <li>serverURI -- defaults to http://localhost:8080/c2id</li>
	 *     <li>apiToken -- defaults to ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6</li>
	 * </ul>
	 *
	 * @return The server details.
	 */
	public static ServerDetails fromSystemProperties() {

		Properties props = System.getProperties();

		URI serverURI;

		try {
			serverURI = new URI(props.getProperty("serverURI", "http://localhost:8080/c2id"));

		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		BearerAccessToken token = new BearerAccessToken(props.getProperty("apiToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6"));

		return new ServerDetails(serverURI, token);
	}


	@Override
	public String toString() {

		return "[serverURI=" + baseURI + ", apiToken=" + apiAccessToken + "]";
	}
}
