package com.nimbusds.openid.connect.provider.benchmarks;


import java.net.URI;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;

import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.client.ClientDeleteRequest;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationErrorResponse;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationResponse;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.rp.*;


/**
 * OpenID Connect client registration benchmark.
 */
@State(Scope.Benchmark)
public class ClientRegistrationBenchmark extends AbstractBenchmark {


	private OIDCClientInformation clientInfo;


	@TearDown
	public void tearDown()
		throws Exception {

		deleteClient(clientInfo.getID());
	}


	/**
	 * Registers a new OpenID Connect client.
	 *
	 * @return The registered OpenID Connect client information.
	 */
	public static OIDCClientInformation registerClient()
		throws Exception {

		OIDCClientMetadata metadata = new OIDCClientMetadata();
		metadata.setGrantTypes(new HashSet<>(Arrays.asList(
			GrantType.AUTHORIZATION_CODE,
			GrantType.IMPLICIT,
			GrantType.REFRESH_TOKEN)));
		metadata.setRedirectionURI(new URI("https://client.com/cb"));
		metadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC);
		metadata.setName("My application");
		metadata.setLogoURI(new URI("https://client.com/logo.png"));
		metadata.setTermsOfServiceURI(new URI("https://client.com/tos.html"));
		metadata.setPolicyURI(new URI("https://client.com/privacy.html"));

		OIDCClientRegistrationRequest regClientRequest = new OIDCClientRegistrationRequest(
			SERVER_DETAILS.clientRegEndpoint,
			metadata,
			SERVER_DETAILS.apiAccessToken);

		HTTPRequest httpRequest = regClientRequest.toHTTPRequest();
		HTTPResponse httpResponse = httpRequest.send();

		ClientRegistrationResponse regResponse = OIDCClientRegistrationResponseParser.parse(httpResponse);

		if (regResponse instanceof ClientRegistrationErrorResponse) {
			throw new Exception("Client registration request failed: " + ((ClientRegistrationErrorResponse) regResponse).getErrorObject().getCode());
		}

		OIDCClientInformationResponse regSuccessResponse = (OIDCClientInformationResponse)regResponse;

		return regSuccessResponse.getOIDCClientInformation();
	}


	/**
	 * Deletes a client's registration.
	 *
	 * @param regURI   The client registration URI.
	 * @param regToken The client registration access token.
	 */
	private static void deleteClient(final URI regURI,
					 final BearerAccessToken regToken)
		throws Exception {

		ClientDeleteRequest request = new ClientDeleteRequest(regURI, regToken);

		HTTPRequest httpRequest = request.toHTTPRequest();

		HTTPResponse httpResponse = httpRequest.send();

		if (httpResponse.getStatusCode() != 204) {
			throw new Exception("Client delete request failed with HTTP status " + httpResponse.getStatusCode());
		}
	}


	/**
	 * Deletes a client's registration.
	 *
	 * @param clientID The client identifier.
	 */
	public static void deleteClient(final ClientID clientID)
		throws Exception {

		URI regURI = new URI(SERVER_DETAILS.clientRegEndpoint + "/" + clientID);

		deleteClient(regURI, SERVER_DETAILS.apiAccessToken);
	}


	/**
	 * Benchmarks OpenID Connect client registration and deletion.
	 */
	@Benchmark
	@BenchmarkMode({Mode.Throughput, Mode.AverageTime, Mode.SampleTime})
	@Measurement(time=60, timeUnit=TimeUnit.SECONDS)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void register()
		throws Exception {

		clientInfo = registerClient();

		deleteClient(clientInfo.getID());
	}
}
