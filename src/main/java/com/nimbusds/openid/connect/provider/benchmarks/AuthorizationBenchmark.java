package com.nimbusds.openid.connect.provider.benchmarks;


import java.net.URL;

import com.nimbusds.common.id.SID;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.http.CommonContentTypes;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.*;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import net.minidev.json.JSONObject;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;


/**
 * Base class for benchmarks using the OAuth 2.0 authorisation endpoint.
 */
@org.openjdk.jmh.annotations.State(org.openjdk.jmh.annotations.Scope.Thread)
class AuthorizationBenchmark extends AbstractBenchmark {


	OIDCClientInformation clientInfo;


	@Setup
	public void setup()
		throws Exception {

		clientInfo = ClientRegistrationBenchmark.registerClient();
	}


	@TearDown
	public void tearDown()
		throws Exception {

		ClientRegistrationBenchmark.deleteClient(clientInfo.getID());
	}


	/**
	 * Starts a new Connect2id authorisation session.
	 *
	 * @param authRequest The OpenID Connect authentication request.
	 */
	static JSONObject startAuthzSession(final AuthenticationRequest authRequest)
		throws Exception {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("query", authRequest.toQueryString());

		HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.POST, SERVER_DETAILS.authzSessionEndpoint.toURL());
		httpRequest.setContentType(CommonContentTypes.APPLICATION_JSON);
		httpRequest.setQuery(jsonObject.toJSONString());
		httpRequest.setAuthorization(SERVER_DETAILS.apiAccessToken.toAuthorizationHeader());

		HTTPResponse httpResponse = httpRequest.send();

		if (httpResponse.getStatusCode() != 200) {
			throw new Exception("Authorization session start request failed with status " + httpResponse.getStatusCode());
		}

		return httpResponse.getContentAsJSONObject();
	}


	/**
	 * Submits a required subject authentication for the specified
	 * Connect2id authorisation session.
	 *
	 * @param authzSessionSID The authorisation session identifier.
	 *
	 * @return The Connect2id authorisation session response.
	 */
	static JSONObject submitSubjectAuthentication(final SID authzSessionSID)
		throws Exception {

		URL url = new URL(SERVER_DETAILS.authzSessionEndpoint + "/" + authzSessionSID);

		Subject sub = new Subject(8); // generate random subject ID

		JSONObject subjectAuth = new JSONObject();
		subjectAuth.put("sub", sub);

		HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.PUT, url);
		httpRequest.setContentType(CommonContentTypes.APPLICATION_JSON);
		httpRequest.setQuery(subjectAuth.toJSONString());
		httpRequest.setAuthorization(SERVER_DETAILS.apiAccessToken.toAuthorizationHeader());

		HTTPResponse httpResponse = httpRequest.send();

		if (httpResponse.getStatusCode() != 200) {
			throw new Exception("Authorization session submit subject auth request failed with status " + httpResponse.getStatusCode());
		}

		return httpResponse.getContentAsJSONObject();
	}


	/**
	 * Retrieves a user's information.
	 *
	 * @param token  The access token for the UserInfo endpoint.
	 *
	 * @return The user information.
	 */
	public static UserInfo getUserInfo(final BearerAccessToken token)
		throws Exception {

		UserInfoRequest request = new UserInfoRequest(SERVER_DETAILS.userInfoEndpoint, token);

		HTTPRequest httpRequest = request.toHTTPRequest();

		HTTPResponse httpResponse = httpRequest.send();

		UserInfoSuccessResponse successResponse = UserInfoSuccessResponse.parse(httpResponse);

		return successResponse.getUserInfo();
	}


	/**
	 * Submits a token request.
	 *
	 * @param clientInfo The OpenID Connect client information.
	 * @param grant      The authorisation grant.
	 *
	 * @return The OpenID Connect access token response.
	 */
	static OIDCTokenResponse submitTokenRequest(final OIDCClientInformation clientInfo,
							  final AuthorizationGrant grant)
		throws Exception {

		ClientAuthentication basicAuth = new ClientSecretBasic(clientInfo.getID(), clientInfo.getSecret());
		TokenRequest tokenRequest = new TokenRequest(
			SERVER_DETAILS.tokenEndpoint,
			basicAuth, grant);

		HTTPRequest httpRequest = tokenRequest.toHTTPRequest();

		HTTPResponse httpResponse = httpRequest.send();

		if (! httpResponse.indicatesSuccess()) {
			throw new Exception("Token request failed with status " + httpResponse.getStatusCode());
		}

		return (OIDCTokenResponse) OIDCTokenResponseParser.parse(httpResponse);
	}
}
