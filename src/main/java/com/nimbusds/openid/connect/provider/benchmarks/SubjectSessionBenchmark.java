package com.nimbusds.openid.connect.provider.benchmarks;


import java.net.URL;
import java.util.concurrent.TimeUnit;

import com.nimbusds.common.id.SID;
import com.nimbusds.oauth2.sdk.http.CommonContentTypes;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.Subject;
import net.minidev.json.JSONObject;
import org.openjdk.jmh.annotations.*;


/**
 * Subject (end-user) session store benchmark.
 */
@State(Scope.Thread)
public class SubjectSessionBenchmark extends AbstractBenchmark {


	/**
	 * Adds a new generated subject session.
	 *
	 * @return The identifier for the added subject session.
	 */
	private static SID addSubjectSession()
		throws Exception {

		Subject sub = new Subject(8);

		JSONObject session = new JSONObject();
		session.put("sub", sub);

		URL resource = new URL(SERVER_DETAILS.subjectSessionEndpoint.toString() + "/sessions");

		HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.POST, resource);
		httpRequest.setContentType(CommonContentTypes.APPLICATION_JSON);
		httpRequest.setAuthorization(SERVER_DETAILS.apiAccessToken.toAuthorizationHeader());
		httpRequest.setQuery(session.toJSONString());

		HTTPResponse httpResponse = httpRequest.send();

		if (! httpResponse.indicatesSuccess()) {
			throw new Exception("Session add failed with status " + httpResponse.getStatusCode());
		}

		return new SID(httpResponse.getHeader("SID"));
	}


	/**
	 * Deletes a subject session.
	 *
	 * @param sid The session identifier.
	 */
	private static void deleteSubjectSession(final SID sid)
		throws Exception {

		URL resource = new URL(SERVER_DETAILS.subjectSessionEndpoint.toString() + "/sessions");
		HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.DELETE, resource);
		httpRequest.setAuthorization(SERVER_DETAILS.apiAccessToken.toAuthorizationHeader());
		httpRequest.setHeader("SID", sid.toString());

		HTTPResponse httpResponse = httpRequest.send();

		if (! httpResponse.indicatesSuccess()) {
			throw new Exception("Session delete failed with status " + httpResponse.getStatusCode());
		}
	}


	/**
	 * Benchmarks session addition followed by deletion.
	 */
	@Benchmark
	@BenchmarkMode({Mode.Throughput, Mode.AverageTime, Mode.SampleTime})
	@Measurement(time=60, timeUnit=TimeUnit.SECONDS)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void addAndDeleteSubjectSessions()
		throws Exception {

		SID sid = addSubjectSession();
		deleteSubjectSession(sid);
	}
}
