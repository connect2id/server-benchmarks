package com.nimbusds.openid.connect.provider.benchmarks;


import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.nimbusds.common.id.SID;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.http.CommonContentTypes;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import com.nimbusds.openid.connect.sdk.AuthenticationSuccessResponse;
import com.nimbusds.openid.connect.sdk.Nonce;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import net.minidev.json.JSONObject;
import org.openjdk.jmh.annotations.*;


/**
 * OpenID Connect authentication / OAuth 2.0 authorisation benchmark.
 *
 * <ul>
 *     <li>Grant type: authorisation code</li>
 *     <li>Authorisation: long-lived (persisted)</li>
 * </ul>
 */
@org.openjdk.jmh.annotations.State(org.openjdk.jmh.annotations.Scope.Thread)
public class AuthorizationCodeGrantBenchmark extends AuthorizationBenchmark {


	/**
	 * Creates a new OpenID Connect authentication request for the
	 * specified client.
	 *
	 * @param clientInfo The OpenID Connect client information.
	 *
	 * @return The OpenID Connect authentication request.
	 */
	private static AuthenticationRequest createOIDCAuthRequest(final OIDCClientInformation clientInfo)
		throws Exception {

		return new AuthenticationRequest(
			new URI("https://c2id.com/login"),
			new ResponseType("code"),
			Scope.parse("openid email profile address offline_access"),
			clientInfo.getID(),
			clientInfo.getOIDCMetadata().getRedirectionURIs().iterator().next(),
			new State(),
			new Nonce());
	}


	/**
	 * Submits a required subject consent for the specified Connect2id
	 * authorisation session.
	 *
	 * @param authzSessionSID The authorisation session identifier.
	 *
	 * @return The OpenID Connect authentication success response.
	 */
	private static AuthenticationSuccessResponse submitSubjectConsent(final SID authzSessionSID)
		throws Exception {

		URL url = new URL(SERVER_DETAILS.authzSessionEndpoint + "/" + authzSessionSID);

		JSONObject consent = new JSONObject();
		consent.put("scope", Scope.parse("openid email profile address offline_access").toStringList());
		consent.put("claims", Arrays.asList("email", "email_verified", "name", "given_name", "family_name", "address"));
		consent.put("long_lived", true);

		JSONObject refreshTokenSpec = new JSONObject();
		refreshTokenSpec.put("issue", true);
		consent.put("refresh_token", refreshTokenSpec);

		JSONObject accessTokenSpec = new JSONObject();
		accessTokenSpec.put("lifetime", 3600L);
		accessTokenSpec.put("encoding", "SELF_CONTAINED");
		consent.put("access_token", accessTokenSpec);

		HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.PUT, url);
		httpRequest.setContentType(CommonContentTypes.APPLICATION_JSON);
		httpRequest.setQuery(consent.toJSONString());
		httpRequest.setAuthorization(SERVER_DETAILS.apiAccessToken.toAuthorizationHeader());

		HTTPResponse httpResponse = httpRequest.send();

		if (httpResponse.getStatusCode() != 302) {
			throw new Exception("Authorization session submit consent request failed with status " + httpResponse.getStatusCode());
		}

		URI location = httpResponse.getLocation();

		return AuthenticationSuccessResponse.parse(location);
	}


	/**
	 * Benchmarks an OpenID Connect login / authorisation.
	 */
	@Benchmark
	@BenchmarkMode({Mode.Throughput, Mode.AverageTime, Mode.SampleTime})
	@Measurement(time=60, timeUnit=TimeUnit.SECONDS)
	@OutputTimeUnit(TimeUnit.SECONDS)
	public void authorize()
		throws Exception {

		AuthenticationRequest oidcAuthRequest = createOIDCAuthRequest(clientInfo);

		JSONObject loginForm = startAuthzSession(oidcAuthRequest);

		SID authzSessionSID = new SID((String)loginForm.get("sid"));
		JSONObject consentForm = submitSubjectAuthentication(authzSessionSID);

		AuthenticationSuccessResponse authzSuccess = submitSubjectConsent(authzSessionSID);

		AuthorizationCode code = authzSuccess.getAuthorizationCode();

		AuthorizationGrant grant = new AuthorizationCodeGrant(
			code,
			clientInfo.getOIDCMetadata().getRedirectionURIs().iterator().next());

		OIDCTokenResponse tokenResponse = submitTokenRequest(clientInfo, grant);

		SignedJWT idToken = (SignedJWT)tokenResponse.getOIDCTokens().getIDToken();

		BearerAccessToken accessToken = tokenResponse.getTokens().getBearerAccessToken();
	}
}
