Connect2id Server Benchmarks

README

Set of benchmarks for the Connect2id server, based on the JMH framework. The
following operations are covered:

 * OpenID Connect client registration.

 * End-user session creation and deletion.

 * OpenID Connect authentication and OAuth 2.0 authorisation.


To run the benchmark:


1. Compile and package the benchmark JAR:

   mvn clean install


2. Run the benchmarks with

   java -DserverURI=[url] -DapiToken=[token] \
   -jar target/benchmarks.jar -wi [n] -i [n] -f [n] -t [n]

   where

      -DserverURI=[value] -- the base Connect2id server URI, defaults to
      http://localhost:8080/c2id if omitted

      -DapiToken=[value] -- the master web API access token, defaults to
      ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6 if omitted

      -wi [n] -- number of warm-up iterations

      -i [n] -- number of iterations (each takes 60 seconds)

      -f [n] -- indicates how many JVMs should be forked (none recommended)

      -t [n] -- number of threads to run (should match the CPU core number)



Examples:

Run the benchmarks against a stock Connect2id server installed on localhost
using 4 threads:

    java -jar target/benchmarks.jar -wi 0 -i 1 -f 0 -t 4


Run the benchmarks against a stock Connect2id server installed on
https://demo.c2id.com/c2id

    java -DserverURI="https://demo.c2id.com/c2id" -jar target/benchmarks.jar -wi 0 -i 1 -f 0 -t 4


Example results against a 4 core server @2.4GHz with 8 GB RAM:

    Benchmark                                               Mode  Samples    Score   Error   Units
    AuthorizationBenchmark.authorize                        thrpt        1  324.024 +/-   NaN   ops/s
    ClientRegistrationBenchmark.register                    thrpt        1    1.332 +/-   NaN  ops/ms
    SubjectSessionBenchmark.addAndDeleteSubjectSessions     thrpt        1    1.709 +/-   NaN  ops/ms
    AuthorizationBenchmark.authorize                         avgt        1    0.056 +/-   NaN    s/op
    ClientRegistrationBenchmark.register                     avgt        1   14.911 +/-   NaN   ms/op
    SubjectSessionBenchmark.addAndDeleteSubjectSessions      avgt        1   11.722 +/-   NaN   ms/op
    AuthorizationBenchmark.authorize                       sample    21841    0.055 +/- 0.001    s/op
    ClientRegistrationBenchmark.register                   sample    90831   13.212 +/- 0.387   ms/op
    SubjectSessionBenchmark.addAndDeleteSubjectSessions    sample    99570   12.060 +/- 0.092   ms/op




This benchmark suite is provided under the terms of the Apache 2.0 licence.

Questions or comments? Email support@connect2id.com

2015-03-14

